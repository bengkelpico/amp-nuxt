module.exports = {
  apps: [
    {
      name: 'amp.dokternet',
      script: './node_modules/.bin/nuxt',

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      args: 'start',
      instances: 'max',
      autorestart: true,
      watch: false,
      max_memory_restart: '256M',
      env: {
        NODE_ENV: 'production',
        PORT: 4000
      }
    },
    {
      name: 'amp-dev',
      script: './node_modules/.bin/nuxt',

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      args: 'dev',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1024M',
      env: {
        NODE_ENV: 'development',
        PORT: 4000
      }
    }
  ]
}
