import incstr from 'incstr'

// region CSS Scope Minify
const createUniqueIdGenerator = () => {
  const index = {}

  const generateNextId = incstr.idGenerator({
    // Removed "d" letter to avoid accidental "ad" construct.
    // @see https://medium.com/@mbrevda/just-make-sure-ad-isnt-being-used-as-a-class-name-prefix-or-you-might-suffer-the-wrath-of-the-558d65502793
    // NOTE: allow "d" letter due to combination of UPPERCASES-lowercases
    alphabet: 'abcdefghijklmnopqrstuvwxyz0123456789_-'
  })

  return (name) => {
    if (index[name]) {
      return index[name]
    }

    let nextId

    do {
      // Class name cannot start with a number.
      nextId = generateNextId()
    } while (/^[0-9_-]/.test(nextId))

    index[name] = generateNextId()
    // console.log(`${name} has id = ${index[name]}`);

    return index[name]
  }
}

const idLocal = createUniqueIdGenerator()
const idComponent = createUniqueIdGenerator()
const Unique = (localName, resourcePath) => {
  const componentName = resourcePath
    .split('/')
    .slice(-2)
    .join('/')
  return idComponent(componentName).toUpperCase() + idLocal(localName)
}

export default {
  mode: 'universal',
  router: {
    middleware: ['amp']
  },
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/amp'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    loaders: {
      cssModules: {
        modules: {
          getLocalIdent: (context, localIdentName, localName) => {
            let result = Unique(localName, context.resourcePath)
            if (process.env.NODE_ENV !== 'production' && localIdentName) {
              result = `${localName}__${result}`
            }
            return result
          }
        }
      }
    },
    extend(config, ctx) {}
  }
}
